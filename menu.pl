% Podstawy Sztucznej Inteligencji
%
% System ekspertowy
%
% L. Rosol
% M. Przybyla

% Menu ktore uzyskuje odpowiedz 'tak' lub 'nie'

get_yes_or_no(Result) :- get(Char),              
                         get0(_),              
                         interpret(Char,Result),
                         !.     

get_yes_or_no(Result) :- nl,
                         write('Odpowiedz T(ak), lub N(ie):'),
                         get_yes_or_no(Result).

interpret(84,yes).  % ASCII 84  = 'T'
interpret(116,yes). % ASCII 116 = 't'
interpret(78,no).   % ASCII 78  = 'N'
interpret(110,no).  % ASCII 110 = 'n'
