% Podstawy Sztucznej Inteligencji
%
% System ekspertowy
%
% L. Rosol
% M. Przybyla

%
% Pytania
%

ask_question(starter_was_ok) :-
   write('Czy kiedy pierwszy raz uruchmiales silnik,'),nl,
   write('rozrusznik pracowal poprawnie? '),nl.

ask_question(starter_is_ok) :-
   write('Czy teraz rozrusznik pracuje normalnie?'),nl.

ask_question(fuel_is_ok) :-
   write('Spojrz na gaznik. Czy widzisz lub czujesz benzyne?'),nl.

