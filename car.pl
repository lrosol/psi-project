% Podstawy Sztucznej Inteligencji
%
% System ekspertowy
%
% L. Rosol
% M. Przybyla


%
% Wczytywanie zewnetrznych plikow
%
:- consult('menu.pl').
:- consult('questions.pl').
:- consult('explains.pl').

%
% Glowne procedury
%

start :-
   write('Program diagnozuje dlaczego samochod nie chce odpalic'),nl,
   write('Odpowiadaj na pytania uzywajac T jako \'tak\', lub N jako \'nie\'.'),nl,
   clear_stored_answers,
   try_all_possibilities.

try_all_possibilities :-
   defect_may_be(D),
   explain(D),
   fail.

try_all_possibilities.


%
% Baza wiedzy mozliwych diagnoz
%   (ponizej stany spelniajace dana diagnoze)
%

defect_may_be(drained_battery) :-
   user_says(starter_was_ok,yes),
   user_says(starter_is_ok,no).

defect_may_be(wrong_gear) :-
   user_says(starter_was_ok,no).

defect_may_be(starting_system) :-
   user_says(starter_was_ok,no).

defect_may_be(fuel_system) :-
   user_says(starter_was_ok,yes),
   user_says(fuel_is_ok,no).

defect_may_be(ignition_system) :-
   user_says(starter_was_ok,yes),
   user_says(fuel_is_ok,yes).


%
% Baza wiedzy przypadkow
%   (Informacje dostarczone przez uzytkownika w trakcie dzialania programu)
%

:- dynamic(stored_answer/2).


%
% Procedura czyszczaca przechowywane odpowiedzi
% bez zniesienia dynamicznych deklaracji
%

clear_stored_answers :- retract(stored_answer(_,_)),fail.
clear_stored_answers.


%
% Procedura przetwarzania odpwowiedzi od uzytkownika,
% zadaje pytanie jesli uzytkownik jeszcze nie odpowiedzial
%

user_says(Q,A) :- stored_answer(Q,A).

user_says(Q,A) :- \+ stored_answer(Q,_),
                  nl,nl,
                  ask_question(Q),
                  get_yes_or_no(Response),
                  asserta(stored_answer(Q,Response)),
                  Response = A.
