% Podstawy Sztucznej Inteligencji
%
% System ekspertowy
%
% L. Rosol
% M. Przybyla

%
%  Wyjasnienia dla roznych diagnoz
%

explain(wrong_gear) :-
   nl,
   write('Sprawdz czy skrzynia biegow jest ustawiona w pozycji parkowania, lub neutralnej'),nl,
   write('Sprobuj zmienic dzwignie zmiany biegow'),nl.

explain(starting_system) :-
   nl,
   write('Sprawdz napiecie akumulatora,'),nl,
   write('regulator, lub alternator; jesli ktorekolwiek powoduje'),nl,
   write('problem, naladuj akumulator lub wymien wadliwy element'),nl.

explain(drained_battery) :-
   nl,
   write('Twoje proby uruchomienia samochody spowodowaly rozladowanie akumulatora'),nl,
   write('Naladowanie lub rozruch z zewnetrzego zrodla jest konieczne'),nl,
   write('Ale niekoniecznie musi byc to wina twojego akumulatora'),nl.

explain(fuel_system) :-
   nl,
   write('Sprawdz czy masz paliwo w baku.'),nl,
   write('Jesli tak, sprawdz czy masz zatkany filtr paliwa'),nl,
   write('lub wadliwa pompe paliwa'),nl.

explain(ignition_system) :-
   nl,
   write('Sprawdz swiece zaplonowe, kable, aparat zaponowy,'),nl,
   write('i inne elementy ukladu zaplonowego'),nl,
   write('Jesli ktorykolwiek z elementow jest wyraznie uszkodzony, lub'),nl,
   write('dawno nie wymieniany, wymien go; jesli to'),nl,
   write('nie rozwiaze problemu, skonsultuj sie z mechanikiem.'),nl.
